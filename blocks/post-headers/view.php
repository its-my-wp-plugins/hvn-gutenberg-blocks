<?php
/**
 * Post headers.
 *
 * @param array $block The block settings and attributes.
 */
if (!is_admin()) :

    $post_type = get_field('post_type');
    $number_posts = get_field('number_posts');
    if(empty($number_posts)) {
        $number_posts = 6;
    }
    $articles = new WP_Query(array(
        'posts_per_page' => $number_posts,
        'post_type' => $post_type,
        'post_status' => array('publish'),
        'orderby' => 'date',
        'order' => 'DESC'
    ));

?>
    <?php if (!empty($articles) && $articles->have_posts()) : ?>
    <div class="hvngb-post-headers-grid">
        <?php while ($articles->have_posts()) : $articles->the_post(); ?>
            <div class="hvngb-post-headers-grid__item" style="background-color:<?= sprintf('#%06X', mt_rand(0, 0xFFFFFF)) ?>">
                <p class="hvngb-post-headers-grid__item-title"><?= get_the_title() ?></p>
            </div>
        <?php endwhile; ?>
    </div>
    <?php endif;
    wp_reset_postdata(); ?>
<?php
else:
    ?>
    <h2 style="font-family: inherit">Post headers</h2>
    <?php
endif;
