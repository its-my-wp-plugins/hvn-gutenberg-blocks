<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_action( 'enqueue_block_editor_assets', 'hvngb_gb_post_headers_editor_assets' );

function hvngb_gb_post_headers_editor_assets() {

    $path = untrailingslashit( __DIR__ );
    $url  = untrailingslashit( plugins_url( '', __FILE__ ) );
    $asset_file = "{$path}/build/index.asset.php";

    // If the asset file exists, get its data and load the script.
    if ( file_exists( $asset_file ) ) {
        $asset = include $asset_file;

        wp_enqueue_script(
            'gb-post-headers',
            "{$url}/build/index.js",
            $asset['dependencies'],
            $asset['version'],
            true
        );

		wp_enqueue_style( 'gb-post-headers', "{$url}/build/style-index.css" );
    }
}

add_action( 'wp_enqueue_scripts', 'hvngb_gb_post_headers_block_assets' );

function hvngb_gb_post_headers_block_assets(){
    $url  = untrailingslashit( plugins_url( '', __FILE__ ) );
	wp_enqueue_style( 'gb-post-headers', "{$url}/build/style-index.css" );
}
