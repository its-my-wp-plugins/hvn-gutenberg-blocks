import { registerBlockVariation } from '@wordpress/blocks';

const MY_VARIATION_NAME = 'hvn/gb_post_headers';

registerBlockVariation( 'core/query', {
    name: MY_VARIATION_NAME,
    title: 'Headings List',
    description: 'Displays a list of post headings',
    isActive: ( ['namespace'] ),
    icon: 'editor-ul',
    attributes: {
        namespace: MY_VARIATION_NAME,
        query: {
            perPage: 6,
            postType: 'page',
            order: 'desc',
            orderBy: 'date',
        },
    },
    scope: [ 'inserter' ],
	allowedControls: [ 'postType' ],
	innerBlocks: [
		[
			'core/post-template',
			{className: 'hvngb-gb-post-headers-grid'},
			[
				[ 'core/post-title', {className: 'hvngb-gb-post-headers-grid__item-title'} ],
			],
		]
	]
});
