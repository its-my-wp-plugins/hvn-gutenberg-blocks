/**
 * WordPress dependencies
 */
import {
    __experimentalBoxControl as BoxControl,
    __experimentalToolsPanel as ToolsPanel,
    __experimentalToolsPanelItem as ToolsPanelItem,
    __experimentalUnitControl as UnitControl,
	ColorPicker
} from '@wordpress/components';

/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-block-editor/#useblockprops
 */
import { useBlockProps, RichText, NumberControl, InspectorControls } from '@wordpress/block-editor';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/#edit
 *
 * @return {Element} Element to render.
 */

export default function Edit({attributes, setAttributes}) {
	const blockProps = useBlockProps({
		className: 'hvngb-text-block',
		style: {
			width: attributes.width,
			color: attributes.textColor
		}
	})

    const resetAll = () => {
        setAttributes( {...attributes, width: undefined, textColor: undefined} );
    };

	return (
		<div {...blockProps}>
			<InspectorControls>
				<ToolsPanel label={ __( 'Style settings' ) } resetAll={ resetAll }>
					<ToolsPanelItem
						hasValue={ () => !! attributes.width }
						label={ __( 'Width' ) }
						onDeselect={ () => setAttributes( {...attributes, width: undefined} ) }
						isShownByDefault
					>
						<UnitControl
							label={ __( 'Width' ) }
							onChange={ (width) => {setAttributes( {...attributes, width} )} }
							value={ attributes.width }
						/>
					</ToolsPanelItem>
					<ToolsPanelItem
						hasValue={ () => !! attributes.textColor }
						label={ __( 'Text color' ) }
						onDeselect={ () => setAttributes( {...attributes, textColor: undefined} ) }
						isShownByDefault
					>
						<label>Color</label>
						<ColorPicker
							color={attributes.textColor}
							onChange={ (color) => {setAttributes( {...attributes, textColor: color} )} }
						/>
					</ToolsPanelItem>
				</ToolsPanel>
			</InspectorControls>
			<RichText
				tagName='div'
				multiline='p'
				value={attributes.content}
				allowedFormats={['core/text-color', 'core/bold', 'core/italic', 'core/link']}
				onChange={(content) => setAttributes({...attributes, content})}
				placeholder={__('Enter your text')}
			/>
		</div>
	);
}
