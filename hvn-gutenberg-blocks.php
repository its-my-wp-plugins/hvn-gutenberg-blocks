<?php
/**
 * Haven's gutenberg blocks
 *
 * Plugin Name: Haven's gutenberg blocks
 * Plugin URI:  https://no-site.org/
 * Description: Haven's custom gutenberg blocks
 * Version:     1.0
 * Author:      Haven
 * Author URI:  https://no-ste.org/
 * Text Domain: haven
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Invalid request.' );
}

define( 'HVNGB_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'HVNGB_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

$hvngb_block_slugs = array('static-text-block', 'post-headers', 'gb-post-headers');

foreach ($hvngb_block_slugs as $block_slug) {
	$block_init_path = HVNGB_PLUGIN_DIR . '/blocks/' . $block_slug . '/' . $block_slug . '.php';
	if(file_exists($block_init_path)){
		include_once $block_init_path;
	}
}